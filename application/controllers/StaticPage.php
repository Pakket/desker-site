<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StaticPage extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('layouts/header');
		$this->load->view('home');
		$this->load->view('layouts/footer');
	}

	public function logout()
	{
		$url = API . 'auth/logout?key=demo&domain=' . API_DOMAIN;
		$req = file_get_contents($url);
		session_destroy();
		redirect('/');
	}

	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			// Load login form
			$this->load->view('layouts/header');
			$this->load->view('login');
			$this->load->view('layouts/footer');
		} else {
			// Call API to create a new user with the submitted data
			$user = $this->input->post(NULL, TRUE);

			// Authorization API
			$url = API . 'auth/login?key=demo&domain=' . API_DOMAIN;
			$req = curl_init($url);
			curl_setopt($req, CURLOPT_POST, true);
			curl_setopt($req, CURLOPT_POSTFIELDS, $user);
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			$auth = json_decode(curl_exec($req), true);

			// If the API returns a failure, drop an error message with it's validations
			if (isset($auth['errors'])) {
				$this->session->set_flashdata('error', $auth['errors']);
				$this->load->view('layouts/header');
				$this->load->view('login');
				$this->load->view('layouts/footer');
			} else {
				// Set the session by the auth
				$this->session->set_userdata('access_token', $auth['access_token']);

				// Redirect to the application
				redirect(APP);
			}
		}
	}

	public function signup()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'GET') {
			// Load signup form
			$this->load->view('layouts/header');
			$this->load->view('signup');
			$this->load->view('layouts/footer');
		} else {
			// Call API to create a new user with the submitted data
			$user = $this->input->post(NULL, TRUE);

			// Authorization API
			$url = API . 'auth/signup?key=demo&domain=' . API_DOMAIN;
			$req = curl_init($url);
			curl_setopt($req, CURLOPT_POST, true);
			curl_setopt($req, CURLOPT_POSTFIELDS, $user);
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			$auth = json_decode(curl_exec($req), true);

			// If the API returns a failure, drop an error message with it's validations
			if (isset($auth['errors'])) {
				$this->session->set_flashdata('error', $auth['errors']);
				$this->load->view('layouts/header');
				$this->load->view('signup');
				$this->load->view('layouts/footer');
			} else {
				// Set the session by the auth
				$this->session->set_userdata('access_token', $auth['access_token']);

				// Redirect to the application
				redirect(APP);
			}
		}
	}
}
