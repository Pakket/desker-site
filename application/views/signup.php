<?= form_open('/signup', 'class="form"'); ?>

<div class="form-item">
  <label>First name</label>
  <br>
  <?= form_input(array(
    'type'  => 'text',
    'name'  => 'first_name',
    'placeholder' => 'Firstname',
    'class' => 'input',
    'value' => set_value('first_name')
  )); ?>
</div>

<br>

<div class="form-item">
  <label>Last name</label>
  <br>
  <?= form_input(array(
    'type'  => 'text',
    'name'  => 'last_name',
    'placeholder' => 'Last name',
    'class' => 'input',
    'value' => set_value('last_name')
  )); ?>
</div>

<br>

<div class="form-item">
  <label>Email</label>
  <br>
  <?= form_input(array(
    'type'  => 'email',
    'name'  => 'email',
    'placeholder' => 'Email',
    'class' => 'input',
    'value' => set_value('email')
  )); ?>
</div>

<br>

<div class="form-item">
  <label>Password</label>
  <br>
  <?= form_input(array(
    'type'  => 'password',
    'name'  => 'password',
    'placeholder' => 'Password',
    'class' => 'input'
  )); ?>
</div>

<div class="form-item">
  <label>Confirm password</label>
  <br>
  <?= form_input(array(
    'type'  => 'password',
    'name'  => 'password_confirmation',
    'placeholder' => 'Confirm password',
    'class' => 'input'
  )); ?>
</div>

<br>

<?= form_submit('signup', 'Signup'); ?>

<?= form_close(); ?>
