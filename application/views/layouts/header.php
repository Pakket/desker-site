<ul>
  <li><a href="<?= SITE; ?>/">Home</a></li>

  <?php if ($this->session->has_userdata('access_token')): ?>
    <li><a href="<?= APP; ?>">Get started</a></li>
    <li><a href="<?php base_url(); ?>logout">Logout</a></li>
  <?php else: ?>
    <li><a href="<?php base_url(); ?>signup">Signup</a></li>
    <li><a href="<?php base_url(); ?>login">Login</a></li>
  <?php endif; ?>
</ul>

<?php if ($this->session->has_userdata('access_token')): ?>
  <p>User id: <?= $this->session->access_token; ?></p>
<?php endif; ?>

<?php if ($this->session->flashdata('error')): ?>
  <div class="error">
    <?php foreach($this->session->flashdata('error') as $error): ?>
      <p><?= $error ?></p>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
