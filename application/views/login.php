<?= form_open('/login', 'class="form"'); ?>

<div class="form-item">
  <label>Email</label>
  <br>
  <?= form_input(array(
    'type'  => 'email',
    'name'  => 'email',
    'placeholder' => 'Email',
    'class' => 'input',
    'value' => set_value('email')
  )); ?>
</div>

<br>

<div class="form-item">
  <label>Password</label>
  <br>
  <?= form_input(array(
    'type'  => 'password',
    'name'  => 'password',
    'placeholder' => 'Password',
    'class' => 'input'
  )); ?>
</div>

<br>

<?= form_submit('login', 'Login'); ?>

<?= form_close(); ?>
