<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'StaticPage/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Static pages
$route['logout'] = 'StaticPage/logout';

// Same page forms
switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':
    $route['signup'] = 'StaticPage/signup';
    $route['login'] = 'StaticPage/login';
  break;
  case 'POST':
    $route['signup']['post'] = 'StaticPage/signup';
    $route['login']['post'] = 'StaticPage/login';
  break;
}
